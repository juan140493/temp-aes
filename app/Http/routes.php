<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {return view('welcome');});
Route::get('nuestra-empresa', function () {return view('empresa');});
Route::get('solar', function () {return view('solar');});
Route::get('sustentable', function () {return view('sustentable');});
Route::get('storage-aes', function () {return view('storage');});
Route::get('servicios', function () {return view('servicios');});
Route::get('su-casa', function () {return view('casa');});
Route::get('contactenos', function () {return view('contacto');});
Route::get('nosotros', function () {return view('nosotros');});
Route::post('sendMail', 'MailController@send');