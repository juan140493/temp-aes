<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class MailController extends Controller
{
    public function send(Request $request){    	

		$data = array(
			'name' => $request->input('name'),
			'email' => $request->input('email'),
			'phone' => $request->input('phone'),
			'msg' => $request->input('message')
		);

		Mail::send('mail', $data, function($message) use ($data){
			$message->from($data['email'], $data['name']);
			$message->to(env('MAIL_DESTINATION', 'aes.soluciones@aes.com'));
			$message->subject('AES SOLUCIONES - website');
	    });


    	//Mail::to(env('MAIL_DESTINATION', 'aes.soluciones@aes.com'))->send( new Contact($request->all()) );
    	return redirect('contactenos')->with('msg', 'Mensaje Enviado!');
    }
}
