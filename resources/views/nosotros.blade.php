@extends('template.main')

@section('title', 'Contáctanos')

@section('content')
<header class="us-header">
	<div class="top">
		<a href="{{ url('/') }}" alt="AES soluciones">
			<img src="{{ asset('img/LogoAESSolucionesazul.png') }}" alt="AES Soluciones" id="logo_aes">
		</a>
	</div>
</header>
<section id="main">
	<section class="row us-section1">
		<div class="col s12 m6 l6 xl6 h2">
			<h2>
				Nuestra
				<span>Misión</span>
			</h2>
			<p>
				Mejorar vidas a través de soluciones energéticas seguras, confiables y sostenibles en todos los mercados que servimos.
			</p>
		</div>
		<div class="col s12 m6 l6 xl6 h2">
			<h2>
				Nuestra
				<span>Visión</span>
			</h2>
			<p>
				Nuestra visión es ser la compañía de energía sostenible líder en el mundo que proporciona de manera segura energía confiable y asequible. Hacemos esto aprovechando nuestras plataformas eléctricas exclusivas y el conocimiento de nuestra gente para proporcionar las soluciones de energía e infraestructura que nuestros clientes realmente necesitan. <br><br>
				Nuestra gente comparte la pasión de ayudar a satisfacer las necesidades energéticas actuales y crecientes del mundo, al tiempo que ofrece a las comunidades y países la oportunidad de crecimiento económico debido a la disponibilidad de energía eléctrica confiable y asequible.

			</p>
		</div>
	</section>	
	<section class="row us-section2">
		<div class="col s12 m12 l12 xl12 h2"><h2 id="Valores">Nuestros<span>Valores</span></h2></div>
		<div class="col s12 m6 l4 xl4">
			<div class="data">
				<img src="{{ asset('img/Icons/ic_seguridad.png') }}" alt="Seguridad">
				<span>Seguridad</span>
				<p>
					Siempre pondremos la seguridad en primer lugar: para nuestra gente, contratistas y comunidades. <br><br>
					Nada viene antes de la seguridad en AES. Aprovechamos una de las fuerzas más poderosas del mundo: la electricidad. Nuestra gente arriesga sus vidas cuando vienen a trabajar todos los días y aseguran que las operaciones seguras en nuestras instalaciones en todo el mundo para que cada persona pueda regresar a casa de forma segura es la piedra angular de nuestras actividades y decisiones diarias. Siempre ponemos la seguridad en primer lugar, y medimos nuestro éxito según cuán seguros logremos nuestros objetivos.
				</p>
			</div>
		</div>
		<div class="col s12 m6 l4 xl4">
			<div class="data">
				<img src="{{ asset('img/Icons/ic_integridad.png') }}" alt="Integridad">
				<span>Integridad</span>
				<p>
					Somos honestos, confiables y confiables. La integridad es el núcleo de todo lo que hacemos: cómo nos conducimos e interactuamos y honramos nuestros compromisos con todos nuestros grupos de interés. <br><br>
					Cuando actuamos con integridad, nos ganamos la confianza de nuestros socios comerciales, clientes, accionistas y las personas que viven en las comunidades donde operamos. Honramos nuestros compromisos al hacer lo que decimos y al no hacer promesas que no podemos cumplir. Mantener nuestra reputación requiere un compromiso continuo de todos nosotros para actuar con el más alto estándar de integridad en todas nuestras decisiones comerciales.
				</p>
			</div>
		</div>
		<div class="col s12 m6 l4 xl4">
			<div class="data">
				<img src="{{ asset('img/Icons/ic_agilidad.png') }}" alt="Agilidad">
				<span>Agilidad</span>
				<p>
					Nos movemos con visión, velocidad y flexibilidad para adaptarnos a nuestro dinámico y cambiante mundo. <br><br>
					Nuestro mundo y nuestra industria están cambiando a un ritmo más rápido que nunca. Debemos ser ágiles y seguir evolucionando en nuestro negocio para tener éxito. Agilidad significa que creamos valor moviéndonos rápidamente, anticipando oportunidades y cambiando de dirección según sea necesario para crecer de nuevas maneras y servir mejor a nuestros clientes.
				</p>
			</div>
		</div>
		<div class="col s12 m6 l4 offset-l2 xl4 offset-xl2">
			<div class="data">
				<img src="{{ asset('img/Icons/ic_excelencia.png') }}" alt="Excelencia">
				<span>Excelencia</span>
				<p>
					Nos esforzamos por ser los mejores en todo lo que hacemos y para actuar en niveles de clase mundial. <br><br>
					La excelencia es un objetivo en sí mismo y la forma de lograr ese objetivo. Luchar por la excelencia significa trabajar continuamente para mejorarnos a nosotros mismos y a nuestras operaciones comerciales.
				</p>
			</div>
		</div>
		<div class="col s12 m6 offset-m3 l4 xl4">
			<div class="data">
				<img src="{{ asset('img/Icons/ic_divertido.png') }}" alt="Divertido">
				<span>Divertido</span>
				<p>
					Trabajamos porque el trabajo puede ser divertido, satisfactorio y emocionante. <br><br>
					Disfrutamos de nuestro trabajo y apreciamos la diversión de ser parte de un equipo que está marcando la diferencia y ganando. Divertirse con el trabajo significa saber que lo que trabajamos en cada día tiene un impacto positivo y nos inspira lo que hacemos. Creemos que un lugar de trabajo que apoya el respeto mutuo, el trabajo en equipo y la diversidad de antecedentes y puntos de vista es un lugar de trabajo divertido.
				</p>
			</div>
		</div>
	</section>	
	<section class="row us-section3">
		<div class="col s12 m12 l12 xl12 h2">
			<h2 id="Cultura">Nuestra<span>Cultura</span></h2>
			<p>
				La energía que nuestra gente aporta a lo que hacen es lo que impulsa a AES. En esencia, nuestra cultura es cómo canalizamos esa energía a través de las formas en que trabajamos juntos.
			</p>
		</div>
		<div class="col s12 m3 l3 xl3">
			<div class="data">
				<img src="{{ asset('img/Icons/ic_ganar.png') }}" alt="Ganar">
				<span>Ganar</span>
				<p>Nos esforzamos por ganar en todo lo que hacemos.</p>
			</div>
		</div>
		<div class="col s12 m3 l3 xl3">
			<div class="data">
				<img src="{{ asset('img/Icons/ic_crecer.png') }}" alt="Crecer">
				<span>Crecer</span>
				<p>Nos comprometemos a hacer crecer a nuestra gente, profesional y personalmente.</p>
			</div>
		</div>
		<div class="col s12 m3 l3 xl3">
			<div class="data">
				<img src="{{ asset('img/Icons/ic_equipo.png') }}" alt="Equipo">
				<span>Equipo</span>
				<p>Nos ayudamos mutuamente a través de nuestras líneas de negocio y nuestras funciones de soporte.</p>
			</div>
		</div>
		<div class="col s12 m3 l3 xl3">
			<div class="data">
				<img src="{{ asset('img/Icons/ic_hablar.png') }}" alt="Hablar">
				<span>Hablar</span>
				<p>Decimos lo que pensamos de una manera abierta y constructiva que nos mueve hacia adelante.</p>
			</div>
		</div>
	</section>	
</section>
<style type="text/css"> .menu-icon-open i{color:#1565c0 !important;}</style>
@endsection

@section('JSextra')
	@if (session('msg'))
		<script type="application/javascript"> Materialize.toast('Mensaje Enviado!', 4000) </script>
	@endif
@endsection

