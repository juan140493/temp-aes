@extends('template.main')

@section('title', 'Sustentable')

@section('CSSextra')
  <style>.paragraphs.bg-sus3 p{text-shadow: 0px 0px 5px rgba(0, 0, 0, 0.7)}</style>
@endsection

@section('content')

<header class="sustentable-header bg-head-4">
  <div class="top">
    <a href="{{ url('/') }}">
      <figure>
        <img src="{{ asset('img/LogoAESSolucionesBN.png') }}" alt="AES Soluciones"> 
      </figure>
    </a>
  </div> 
  <h1>
      SUSTENTABLE
  </h1>   
</header>
<section id="main">
  
  <section class="row sustentable-section1 bg-black paragraphs">    
    <div class="col s12 m6 l6 xl5 offset-xl1 valign-wrapper">
      <p>
        Los equipos eléctricos utilizados para iluminación, refrigeración o climatización se han modernizado y por tanto se han vuelto más eficientes en el consumo de la energía.
      </p>
    </div>
    <div class="col s12 m6 l6 xl5 valign-wrapper">
      <p>
        Esto trae muchos beneficios, que nos permiten detectar y diagnosticar de forma temprana fallos que conlleven un aumento drástico del consumo de energía, así como de costo de operación y mantenimiento.
      </p>
    </div>    
  </section>
  <section class="row sustentable-section2">    
    <div class="col s12 m6 l8 xl8 right-align bg-sus2">      
      <canvas height="650" width="350" id="canvas-sustentable1" class="hide-on-small-only"></canvas>
    </div>
    <div class="col s12 m6 l4 xl4 bg-blue">

      <div class="valign-wrapper">
        <img src="{{ asset('img/Icons/foco.png') }}" alt="AES">
        <span class="line-p"></span>
        <p>
          <span>Iluminación eficiente</span>
          AES Soluciones provee servicios integrales de iluminación eficiente, indispensable para alcanzar la sostenibilidad y proveer un alto desempeño en el consumo energético. 
          <br> <br>
          A  partir de una auditoría energética se evalúan las diversas tecnologías de luminarias, de modo tal de ofrecer la propuesta óptima para cada cliente, con lo cual le aseguramos la mejor relación costo beneficio para la disminución de su consumo de energía eléctrica.
        </p>
      </div>
      
    </div>    
  </section>
  <section class="row paragraphs bg-sus3">
    <div class="col s12 m12 l12 xl10 offset-xl1 valign-wrapper center-align">
      <p>
        El  Salvador está atravesando un periodo de cambio de pasar por una conversión del sistema de alumbrado público de mercurio a tecnología de primer mundo LED.
      </p>
    </div>
    <div class="col s12 m6 l6 xl4 valign-wrapper hide">
      <p>
        El inventario de iluminación de ESA se compone de aprox. 350.000 luminarias de alumbrado público, con una estructura de más de 90% de las lámparas de mercurio, de sodio al 5 y 5% fluorescente y otros.
      </p>
    </div>
  </section>
  <section class="row bg-black hide-on-small-only" id="aux-canvas">
    <div class="col s6"></div>
    <div class="col s2 w"></div>
    <div class="col s4"></div>
  </section>
  <section class="row sustentable-section3">
    <div class="col s12 m12 l6 xl6 bg-white">
      <div class="cont-text-img">
        <img src="{{ asset('img/Icons/ducha.png') }}" alt="AES">
        <span class="line-p"></span>
        <p>       
          El proyecto lleva las siguientes etapas: suministro e instalación de luminarias LED,  supervisión y la gestión de este régimen de administración por un período de hasta 10 años a través del cobro de una cuota mensual para el municipio dentro de la factura de energía eléctrica.
          <br>
          Los servicios de AES Soluciones para iluminación pública son altamente especializados, enfocados en la necesidad de las alcaldías y constructoras de modernizar dichos sistemas para disminuir sus consumos de energía eléctrica. 
          <br>
          Nuestro servicio incluye la gestión automatizada del parque lumínico, a través de un sistema de telegestión que controla y gestiona de manera remota cada una de las luminarias que componen el alumbrado público, con este sistema el usuario es capaz de modificar y monitorear los factores eléctricos de cada una de las luminarias.
          <br>
          El sistema es capaz de rastrear de forma individual la ubicación exacta de las luminarias en sincronización con Google Maps. Esto contribuye a mejorar los índices de calidad del servicio, ya que permite la detección de necesidades de mantenimiento correctivo oportunamente, disminuyendo el tiempo de respuesta ante fallas. El sistema también controla de forma remota el encendido y apagado total o parcial del alumbrado.
        </p>
      </div>
    </div>
    <div class="col hide-on-992 s2 m1 l2 xl2 bg-white">
      <canvas id="canvas-sustentable2" height="700" width="300"></canvas>
    </div>
    <div class="col s12 m12 l4 xl4 bg-black">

      <div class="cont-text-img">
        <img src="{{ asset('img/Icons/bateria.png') }}" alt="AES">
        <span class="line-p"></span>
        <p>       
          Con AES Soluciones, las municipalidades pueden optar por un modelo de negocio integrado, que les permite financiar el cambio de alumbrado público por iluminación eficiente, y hacer efectivo su pago con los ahorros mensuales.
          <br><br>
          Estos ahorros están garantizados por los equipos de últimas tecnología que AES El Salvador ha seleccionado.
          <br><br>
          Nuestra experticia nos permite acompañar a las municipalidades desde la detección de las necesidades lumínicas a fin de adquirir los equipos más adecuados, hasta la instalación y mantenimiento preventivo y correctivo.
        </p>
      </div>
      
    </div>
  </section>
  <section class="row bg-sus4 solar-section5">
    <div class="col s12 m11 l8 xl8 bg-semi-inverse">
      <div>
        <img src="{{ asset('img/Icons/aire_2.png') }}" alt="">
        <h2>Aire Acondicionado</h2>
        <br><br>
        <p>
          AES Soluciones, pone a su disposición la línea de Aires Acondicionados, la
          cual consiste en una oferta de productos y servicios de la más alta calidad.
          AES ha buscado alianzas con las empresas fabricantes de  renombre a nivel
          mundial como es LG, YORK, LENNOX, INNOVAIR, etc. permitiendo que los
          clientes tengan acceso a tecnología de primer nivel, con garantías de
          fábrica, excelentes precios y sobre todo el mejor respaldo de una empresa
          como AES puede ofrecer.
          <br><br>
          En AES Soluciones, contamos con personal entrenado para desarrollar tareas
          complejas y poco frecuentes, proporcionamos los servicios que nuestros
          clientes necesitan en el momento oportuno, simplificamos la planificación y
          ponemos a su disposición nuestros conocimientos y equipos buscando hacer
          eficientes sus recursos energéticos.
          <br><br>
          Contáctenos y a través de nuestro equipo le brindaremos la propuesta ideal
          con un servicio personalizado, para ofrecerle las mejores  soluciones a sus
          necesidades, facilitando el equilibrio entre confort de temperatura, ahorro
          y retorno de inversión, garantizamos el cumplimiento de sus requerimientos y
          necesidades de eficiencia energética con el más alto estándar que sólo AES
          puede ofrecerle.
        </p>
      </div>      
    </div>
  </section>
</section>

@endsection
