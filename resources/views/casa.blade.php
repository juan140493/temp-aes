@extends('template.main')

@section('title', 'Su Casa')

@section('content')

<header class="casa-header bg-head-7">
  <div class="top">
    <a href="{{ url('/') }}">
      <figure>
        <img src="{{ asset('img/LogoAESSolucionesBN.png') }}" alt="AES Soluciones"> 
      </figure>
    </a>
  </div> 
  <h1>
    <p>
      SU CASA
    </p>
  </h1>   
</header>
<section id="main">
  

  <section class="row casa-section1">
    <div class="col s12 m6 l5 xl5 bg-white">
      <div class="cont-text-img">
        <img src="{{ asset('img/Icons/aes_seguro.png') }}" alt="AES">
        <h2>AES SEGUROS</h2>
        <p>       
          La venta de productos de seguros puede ser tan exitosa como la venta de cualquier tipo de productos, por lo que nuestro equipo se centró en el desarrollo de micro productos de seguros que prestan los servicios de valor añadido para nuestra base de clientes.
        </p>
      </div>
    </div>
    <div class="col s2 bg-blue hide-on-992">
      <canvas id="canvas-casa1" class="hide-on-1000" height="700" width="300"></canvas>
    </div>
    <div class="col s12 m6 l5 xl5 bg-blue">
      <div class="cont-text-img">
        <img src="{{ asset('img/Icons/aes_salud.png') }}" alt="AES">
        <h2>AES SALUD</h2>
        <p>       
          AES SALUD es un exclusivo programa médico de consultas externas a un bajo costo en clínicas privadas que se brinda únicamente a los clientes de las empresas distribuidoras CAESS, CLESA, EEO y DEUSEM. La afiliación se realiza por vía telefónica y a través de nuestras oficinas comerciales, y está dirigido a mayores de 18 años.
          <br><br>
          <b>Precio al Público:</b>
          <br>
          <ul>
            <li>• Plan AES SALUD $9.99 con IVA incluido</li>
            <li>• Plan AES SALUD Básico $5.00 con IVA incluido</li>
          </ul> <br>
          Para ambos planes la modalidad de cobro es a través de su factura mensual de CAESS, CLESA, EEO o DEUSEM.
        </p>
      </div>      
    </div>
  </section> 

  <section class="row bg-blue" id="aux-canvas">
    <div class="col s5"></div>
    <div class="col s2 w"></div>
    <div class="col s5"></div>
  </section>

  <section class="row casa-section2 bg-blue">    
    <div class="col s12 m12 l6 xl6 valign-wrapper">
      <div class="cont-half">
        <h2>Beneficios del afiliado</h2>
        <ul class="collapsible" data-collapsible="accordion">
          <li>
            <div class="collapsible-header active"><span class="icon-us">+</span><span>Citas médicas</span></div>
            <div class="collapsible-body">
              Consulta externa en clínicas privadas con las especialidades de Medicina General, Ginecología y Pediatría o Cardiología. Podrá acudir para recibir consulta con los médicos de la red autorizada de Salud Global en horas hábiles, con una programación de al menos 12 horas de anticipación.
            </div>
          </li>
          <li>
            <div class="collapsible-header"><span class="icon-us">+</span><span>Traslado Ambulatorio Programado</span></div>
            <div class="collapsible-body">
              En caso que el afiliado requiera de traslado hacia algún centro asistencial, Asistencia Global coordina el envío de personal médico en una ambulancia totalmente equipada.
            </div>
          </li>
          <li>
            <div class="collapsible-header"><span class="icon-us">+</span><span>Orientación médica telefónica</span></div>
            <div class="collapsible-body">
              El afiliado titular y todos sus afiliados adicionales podrán hacer uso del servicio de orientación medica vía telefónica la cual está habilitada las 24 horas del día.
            </div>
          </li>
        </ul>
        <p>
          La inscripción al programa no requiere de exámenes médicos.
        </p>
      </div>
    </div>
    <div class="col s12 m12 l6 xl6 valign-wrapper">
      <div class="cont-half">
        <p>
          La cobertura de los servicios se extiende en todo el territorio de la República del El Salvador, donde exista la infraestructura adecuada y autorizada por Salud Global para poder prestar servicios del plan de asistencia médica. 
          <br><br>
          <b>¿Quiénes son los afiliados a la asistencia?</b> <br>
          El afiliado puede inscribir a 5 miembros, incluyendo al titular, con derecho de hacer uso del servicio de asistencias. En el caso de ser casado, los afiliados serán cónyuge y 3 hijos menores de 18 años. En caso de ser una persona soltera, ésta puede incorporar a sus padres (estos últimos sin restricción de edad). 
          <br><br>
          <b>¿Cuándo inicia la vigencia del servicio?</b> <br>
          La vigencia del servicio inicia veinticuatro (24) horas después de hacer aceptado la afiliación al programa de asistencia médica. 
          <br><br>
          <b>Cancelación de la Asistencia</b> <br> 
          El usuario podrá solicitar la cancelación de la afiliación en cualquier momento, a través del número telefónico 2565-2999
          <br><br>
          Para mayor información y la contratación del servicio llama al teléfono: 2565-2999, de lunes a viernes de horas hábiles. Los servicios médicos son responsabilidad de Asistencia Global.
        </p> 
      </div>     
    </div>    
  </section>

  <section class="row casa-section3">
    <div class="col s12 m5 l5 xl5 bg-white">
      <div class="cont-text-img">        
        <p>       
          El cobro de tu Asistencia AES S.O.S. se realiza automáticamente en la factura de energía eléctrica de CAESS, CLESA, EEO o DEUSEM, la cual puedes pagar en los puntos de pago autorizados (Agencias de CAESS, CLESA, EEO y DEUSEM, bancos del sistema financiero y más de 4.700 puntos de pago a nivel nacional). El pago de servicio no es vinculante con el pago de energía eléctrica. El monto de energía eléctrica puede pagarse independientemente del pago de la Asistencia. 
          <br><br>
          ¿Cuándo inicia la vigencia del servicio? 
          Cuarenta y ocho (48) horas después de haber comprado la asistencia.
          <br><br>
          ¿Quiénes son los beneficios del servicio?
          El titular afiliado al servicio de asistencia AES S.O.S, su cónyuge e hijos menores de 18 años.
        </p>
      </div>
    </div>
    <div class="col s2 bg-green hide-on-small-only">
      <canvas id="canvas-casa2" height="700" width="300"></canvas>
    </div>
    <div class="col s12 m5 l5 xl5 bg-green">
      <div class="cont-text-img">
        <img src="{{ asset('img/Icons/sos.png') }}" alt="AES">
        <h2>Servicio de<br>asistencia AES</h2>
        <p>       
          Con tan solo una llamada telefónica, usted recibirá ayuda inmediata de técnicos y profesionales certificados, que se encargarán de solventar sus emergencias en su vehiculo, casa, durante viajes dentro del país, así como en brindarles asesoría legal.
          <br>
          Asistencias HOGAR, VIAL Y LEGAL, son servicios de AES S.O.S. exclusivos para los clientes de las empresas distribuidoras CAESS, CLESA, EEO y DEUSEM. Se ofrecen únicamente por teléfono, para clientes mayores de 18 años.
          <br>
          Asistencia HOGAR: Cubre tus emergencias de cerrajería, electricidad y plomería, así como emergencias en grifos o vidrios de tu vivienda.
          <br>
          Asistencia VIAL: Te ofrece auxilio en carretera y en tus viajes por todo el país, asistiéndote para llegar a tu destino o regresar a la comodidad de tu casa.
          <br>
          Asistencia LEGAL: Para situaciones o emergencias legales
        </p>
      </div>      
    </div>
  </section>  

  <section class="row casa-section4 bg-gray">    
    <div class="col s12 m12 l6 xl6 valign-wrapper">
      <div class="cont-half">
        <img src="{{ asset('img/Icons/aes_pluz.png') }}" alt="AES">
        <h2>Seguro PLUZ</h2>        
        <p>
          Este seguro cubre gastos mensuales del núcleo familiar en caso que el titular muera en algún tipo de accidente. Es un producto de adhesión voluntaria, exclusivo para los clientes de las empresas distribuidoras CAESS, CLESA, EEO y DEUSEM.
          <br><br>          
          <b>Beneficios:</b> <br><br>
          • Indemnización por muerte accidental: Indemnización inmediata después del fallecimiento del asegurado.<br><br>
          • 48 pagos consecutivos: Mensualmente se entregará a los beneficiarios el monto contratado, el cual puede utilizarse para cubrir gastos cotidianos de la familia como luz, agua, teléfono, alimentos, etc. <br><br>
          • El monto entregado a los beneficiarios es de libre disponibilidad.          
        </p> 
      </div>
    </div>
    <div class="col s12 m12 l6 xl6 valign-wrapper">
      <div class="cont-half">
        <p>
          <b>Beneficios:</b> <br><br>
          • Beneficio de traslado ambulatorio terrestre: El Asegurado tendrá derecho a dos (2) servicios médicos ambulatorios terrestres por año. Podrá gozar de este servicio el asegurado o cualquier miembro de su familia que resida en el mismo domicilio. Para que pueda brindarse este servicio, es indispensable que exista acceso para la ambulancia.<br><br>
          • No tiene deducible.<br><br>
          • Orientación médica telefónica ilimitada las 24 horas del día los 365 días del año con médicos certificados en El Salvador.<br><br>
          • El cobro de la prima se realizará automáticamente en la factura de energía de CAESS, CLESA, EEO o DEUSEM, la cual se puede pagar en los puntos de pago autorizados (Agencias de CAESS, CLESA, EEO o DEUSEM, bancos del sistema financiero y más de 4 700 puntos de pago a nivel nacional).<br><br>
          • El pago del seguro no es vinculante con el pago de la energía eléctrica, es decir, el monto de energía eléctrica puede pagarse independientemente del pago del seguro.
        </p> 
      </div>     
    </div>
  </section>
  
  <section class="row casa-section1">
    <div class="col s12 m6 l5 xl5 bg-white">
      <div class="cont-text-img">        
        <p>       
          ¿Quiénes son los beneficiarios?
          <br><br>
          Son las personas designadas por el asegurado para recibir la suma pactada en el contrato,  en caso que el titular fallezca por un accidente. El certificado de seguro contiene el nombre de la persona que el asegurado designó como beneficiario al momento de la venta telefónica. El asegurado podrá designar hasta un máximo de 4 beneficiarios, en caso que no designe beneficiarios, la indemnización se repartirá entre sus beneficiarios legales. 
          <br><br>
          El o los beneficiarios tendrán un plazo máximo de 30 días hábiles (según póliza) desde la fecha del siniestro, para entregar en Mapfre La Centro Americana la siguiente papelería:
          <br><br>
          • Certificado de Seguro. <br> <br>
          • Formularios de reclamos, los cuales serán proporcionados por la Aseguradora. <br> <br>
          • Original de Certificación de Partida de Nacimiento del asegurado titular.

        </p>
      </div>
    </div>
    <div class="col s2 bg-blue hide-on-992">
      <canvas id="canvas-casa3" class="hide-on-1000" height="700" width="300"></canvas>
    </div>
    <div class="col s12 m6 l5 xl5 bg-blue">
      <div class="cont-text-img">
        <p>       
          • Original de Certificación de la Partida de Defunción del asegurado titular. <br>
          • Original de certificación del Reconocimiento del cadáver, incluyendo autopsia y resultados de pruebas toxicológicas, si fueran realizadas. <br>
          • Fotocopia de Documento Único de Identidad (DUI) del asegurado titular. <br>
          • Fotocopia de Documento Único de Identidad (DUI) del beneficiario (s) mayor (es) de 18 años de edad. <br>
          • Original de Certificación de Partida de nacimiento de beneficiario (s) menor (es) de 18 años. <br>
          • Original de Documento de Nombramiento de tutor definitivo en caso de beneficiario (s) menor (es) de 18 años de edad quede desamparado. <br>
          • Recibos de consumo de energía eléctrica (según plan elegido). <br>          
          <br><br>
          Cancelación del Seguro
          <br><br>
          El asegurado podrá solicitar la cancelación del seguro en cualquier momento a través del número telefónico de Servicio al Cliente: 2211-9200
          <br>
          Este producto cuenta con la aprobación de la Superintendencia del Sistema Financiero de El Salvador, con la póliza AP-1366, aprobada el 03 de Julio de 2009.

        </p>
      </div>      
    </div>
  </section> 

  <section class="row bg-casa2 solar-section5">
    <div class="col s12 m11 l8 xl8 bg-semi-inverse">
      <div class="over">        
        <h2>AES Bombillos</h2>
        <br><br>
        <p>
          AES Soluciones, conociendo lo importante que es la gestión y ahorro
          energético en el consumo del hogar, ponemos a su disposición bombillos con
          tecnología LED de diferentes potencias, los cuales permiten al usuario un
          ahorro sustancial en su factura eléctrica por medio de una utilización de
          una tecnología eficiente en iluminación. Entre las opciones que ofrecemos
          podemos mencionar:
          <br><br>
          • Bombillo LED de 6W los cuales son equivalentes en iluminación a una bombilla de 45W incandescente con un 87% menos de consumo energético.<br>
          • Bombillo LED de 9W los cuales son equivalentes en iluminación a una bombilla de 65W incandescente con un 86% menos de consumo energético.<br><br>
          Estos Bombillos LED están disponibles en todas nuestras oficinas comerciales a nivel nacional, además en todos nuestros productos ofrecemos garantía de un año por defectos de fábrica.
        </p>
      </div>      
    </div>
  </section>

</section>

@endsection
