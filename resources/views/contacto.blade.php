@extends('template.main')

@section('title', 'Contáctanos')

@section('content')
<header class="contact-header">	
	<div class="top hide"><a href="{{ url('/') }}" alt="AES soluciones"><img src="{{ asset('img/LogoAESSolucionesazul.png') }}" alt="AES Soluciones" id="logo_aes"></a></div>	
	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15505.604444519928!2d-89.2419165!3d13.6941395!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc055b4daeda05cab!2sAES+El+Salvador+-+Edificio+Corporativo!5e0!3m2!1ses-419!2ssv!4v1518624776847" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>  
</header>
<section id="main">
	<section class="row contact-section">
		<div class="col hide-on-small-only s12 m12 l6 xl7 bg-contact">
			<div class="t">
				<img src="{{ asset('img/logo_menu.png') }}" alt="AES Soluciones" class="responsive-img">
				<p>
					Somos una compañia que difunde tecnología a través de nuevas formas de uso energético, solar, lumínica, de almacenamiento y de uso residencial. <br>
					¡No lo olvides!
				</p>
			</div>
			<canvas height="700" class="hide-on-700 hide" width="50" id="canvas-contact"></canvas>
		</div>
		<div class="col s12 m12 l6 xl5 bg-semi-white">
			<div class="row">
				<form action="{{ url('sendMail') }}" method="post" id="contactForm" class="col s12">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				  <div class="row">
				    <div class="col s12">
				      <label for="name">Nombre/empresa*</label>
				      <input type="text" class="browser-default" name="name" required>
				    </div>
				  </div>
				  <div class="row">
				    <div class="col s12">
				      <label for="email">Correo electrónico*</label>
				      <input type="email" class="browser-default" required name="email">
				    </div>
				  </div>
				  <div class="row">
				    <div class="col s12">
				      <label for="phone">Teléfono</label>
				      <input type="text" class="browser-default" name="phone">
				    </div>
				  </div>
				  <div class="row">
			        <div class="col s12">
			          <label for="textarea1">Mensaje*</label>
			          <textarea id="textarea1" class="browser-default" rows="10" required name="message"></textarea>
			        </div>
			      </div>
			      <div class="row">
			        <div class="col s12">
			          <button class="waves-effect waves-light blue darken-3 btn" type="submit">Enviar</button>
			        </div>
			      </div>      
				</form>
			</div>
		</div>
	</section>	
</section>
<style type="text/css"> .menu-icon-open i{color:#1565c0 !important;}</style>
@endsection

@section('JSextra')
	@if (session('msg'))
		<script type="application/javascript"> Materialize.toast('Mensaje Enviado!', 4000) </script>
	@endif
@endsection

