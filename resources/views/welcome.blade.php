@extends('template.main')

@section('title', 'Inicio')

@section('content')

<div id="fix" class="fix">
  <div class="midnightHeader default">      
    <img data-size="0.6" class="third" src="{{ asset('img/Flores/flor_morada.png') }}" alt="AES">
    <img data-size="0.8" class="second" src="{{ asset('img/Flores/flor_celeste.png') }}" alt="AES">
    <img class="primary" src="{{ asset('img/Flores/flor_amarilla.png') }}" alt="AES">
    <img data-size="0.8" class="second" src="{{ asset('img/Flores/flor_verde.png') }}" alt="AES">
    <img data-size="0.6" class="third" src="{{ asset('img/Flores/flor_roja.png') }}" alt="AES">
  </div>

  <div class="midnightHeader seccion1 hide-on-med-and-down">
    <img src="{{ asset('img/Flores/flor_amarilla.png') }}" alt="AES">
  </div>
  <div class="midnightHeader seccion2 hide-on-med-and-down">
    <img src="{{ asset('img/Flores/flor_verde.png') }}" alt="AES">
  </div>
  <div class="midnightHeader seccion3 hide-on-med-and-down">
    <img src="{{ asset('img/Flores/flor_celeste.png') }}" alt="AES">
  </div>
  <div class="midnightHeader seccion4 hide-on-med-and-down">
    <img src="{{ asset('img/Flores/flor_morada.png') }}" alt="AES">
  </div>
  <div class="midnightHeader seccion5 hide-on-med-and-down">
    <img src="{{ asset('img/Flores/flor_roja.png') }}" alt="AES">
  </div>
</div>
<div class="fix-small">
  <div class="midnightHeader default">      
    <img class="third" src="{{ asset('img/Flores/flor_morada.png') }}" alt="AES">
    <img class="second" src="{{ asset('img/Flores/flor_celeste.png') }}" alt="AES">
    <img class="primary" src="{{ asset('img/Flores/flor_amarilla.png') }}" alt="AES">
    <img class="second" src="{{ asset('img/Flores/flor_verde.png') }}" alt="AES">
    <img class="third" src="{{ asset('img/Flores/flor_roja.png') }}" alt="AES">
  </div>  
</div>

<header class="home-header bg-head-1">  
  <h1>
    <img src="{{ asset('img/LogoAESSolucionesBN.png') }}" alt="AES Soluciones"> 
  </h1>   
</header>
<section id="main">
  <section data-midnight="seccion1" class="row home odd">
    <div class="col s6 m4 l6 xl6 hide-on-small-only jarallax" data-jarallax data-speed="0.5"> 
      <img class="jarallax-img" src="{{ asset('img/2.jpg') }}" alt="AES">
      <div class="arrow-left"></div> 
    </div><!--
    --><div class="col s12 m8 l6 xl6 cont-text-home">
      <div class="valign-wrapper">
        <p class="home-text">
          <span><a href="{{ url('solar') }}">Solar</a></span>
          Brindamos servicios de diseño, construcción y operación y mantenimiento (O&M) de plantas solares fotovoltaicas.
          <br><br>
          Los servicios de O&M y gestión de activos para plantas fotovoltaicas son ejecutados por un equipo de operadores de nivel mundial con el respaldo de las empresas AES El Salvador.
          Nuestra experiencia global y presencia en todo el territorio salvadoreño, pone a su disposición confiabilidad y capacidad de respuesta en tiempo y recursos, insuperable en el mercado.
          Los contratos de servicio son diseñados según sus necesidades, con plazos y alcance flexibles.
        </p>
      </div>
    </div><!--
    --><div class="col s12 jarallax show-on-small" data-jarallax data-speed="0.5"> 
      <img class="jarallax-img" src="{{ asset('img/2.jpg') }}" alt="AES">      
    </div>
  </section>
  <section data-midnight="seccion2" class="row home">
    <div class="col s12 m8 l6 xl6 cont-text-home">
      <div class="valign-wrapper">
        <p class="home-text">
          <span><a href="{{ url('sustentable') }}">Sustentable</a></span>
          Contamos con una línea de productos y servicios de Iluminación Residencial, Comercial e Industrial con altos estándares de calidad, que le permitirán lograr eficiencia energética a través de tecnologías de primer nivel, garantías de fábrica, excelentes precios y asesoramiento personalizado.
          <br><br>
          Ciudades Sostenibles: Impulsamos el cambio hacia ciudades inteligentes y sostenibles, al ofrecer una solución integral que permite a las alcaldías sustituir sus luminarias tradicionales por tecnología LED.
        </p>
      </div>
    </div><!--
    --><div class="col s12 m4 l6 xl6 jarallax" data-jarallax data-speed="0.5"> 
      <img class="jarallax-img" src="{{ asset('img/3.jpg') }}" alt="AES">
      <div class="arrow-right"></div> 
    </div>
  </section>
  <section data-midnight="seccion3" class="row home odd">
    <div class="col s6 m4 l6 xl6 hide-on-small-only jarallax" data-jarallax data-speed="0.5"> 
      <img class="jarallax-img" src="{{ asset('img/5.jpg') }}" alt="AES">
      <div class="arrow-left"></div> 
    </div><!--
    --><div class="col s12 m8 l6 xl6 cont-text-home">
      <div class="valign-wrapper">
        <p class="home-text">
          <span><a href="{{ url('servicios') }}">Servicios</a></span>
          Nuestros servicios técnicos le garantizan el buen estado funcional de todos los elementos que integran sus equipos en una subestación eléctrica, a través de acciones preventivas, pruebas mecánicas, eléctricas y dielécticas.
          <br><br>
          <b>Mantenimiento preventivo: </b>Cambio o reemplazo de partes en función de un intervalo de tiempo. <br>
          <b>Mantenimiento predictivo: </b>Indica el momento en que las piezas o componentes están próximos a finalizar su vida útil. <br>
          <b>Mantenimiento proactivo: </b> Dirigido fundamentalmente a la detección de las causas que generan el desgaste y que conducen a las fallas.
        </p>
      </div>
    </div><!--
    --><div class="col s12 jarallax show-on-small" data-jarallax data-speed="0.5"> 
      <img class="jarallax-img" src="{{ asset('img/5.jpg') }}" alt="AES">      
    </div>
  </section>
  <section data-midnight="seccion4" class="row home">
    <div class="col s12 m8 l6 xl6 cont-text-home">
      <div class="valign-wrapper">
        <p class="home-text">
          <span><a href="{{ url('su-casa') }}">Su Casa</a></span>
          Usted podrá recibir ayuda inmediata a través de personal calificado, que se encargará de asistirle en sus emergencias de salud y emergencias en casa, además de asistencia vial y legal durante viajes dentro del país.
          <br>
          Nuestros servicios son exclusivos para nuestros clientes de las distribuidoras CAESS, CLESA, EEO y DEUSEM y se ofrecen únicamente por vía telefónica, para clientes mayores de 18 años.
          <br>
          Contáctanos para asistencias de emergencias al 2207-8868 y para asistencias de salud al 2565-2999
        </p>
      </div>
    </div><!--
    --><div class="col s12 m4 l6 xl6 jarallax" data-jarallax data-speed="0.5"> 
      <img class="jarallax-img" src="{{ asset('img/6.jpg') }}" alt="AES">
      <div class="arrow-right"></div> 
    </div>
  </section>
  <section data-midnight="seccion5" class="row home odd">
    <div class="col s6 m4 l6 xl6 hide-on-small-only jarallax" data-jarallax data-speed="0.5"> 
      <img class="jarallax-img" src="{{ asset('img/4.jpg') }}" alt="AES">
      <div class="arrow-left"></div> 
    </div><!--
    --><div class="col s12 m8 l6 xl6 cont-text-home">
      <div class="valign-wrapper">
        <p class="home-text">
          <span><a href="{{ url('storage-aes') }}">Storage <small style="font-size: 12px">(próximamente)</small> </a></span>
          Energy Storage, la plataforma de almacenamiento de energía de AES, otorga respuesta de alta velocidad para entregar energía al sistema en cada instante requerido.
          <br><br>
          Cuenta con estructuras avanzadas de control que ayudan a mejorar la seguridad y confiabilidad, optimizando la operación, incrementando su vida útil y disponibilidad y reduciendo los costos del operador. 
          También facilita la incorporación de las energías renovables al sistema, contribuyendo a la reducción del consumo de combustibles fósiles.
        </p>
      </div>
    </div><!--
    --><div class="col s12 jarallax show-on-small" data-jarallax data-speed="0.5"> 
      <img class="jarallax-img" src="{{ asset('img/4.jpg') }}" alt="AES">      
    </div>
  </section>
</section>

@endsection
