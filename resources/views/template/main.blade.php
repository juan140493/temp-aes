<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
        <title>AES Soluciones - @yield('title')</title>
        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('img/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('img/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('img/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('img/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('img/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/apple-icon-180x180.png') }}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('img/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('img/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('img/manifest.json') }}">
        <meta name="msapplication-TileColor" content="#061c2a">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#061c2a">
        <meta name="description" content="Somos una compañia que difunde tecnología a través de nuevas formas de uso energético, solar, lumínica, de almacenamiento y de uso residencial">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">   
        @yield('CSSextra')     
    </head>
    <body>    
    @yield('content')

    <footer>&copy; {{ date('Y') }} AES El Salvador. Todos los derechos reservados</footer>
    <img src="{{ asset('img/Icons/YINGYANG.png') }}" alt="AES" class="hide">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css" integrity="sha256-e22BQKCF7bb/h/4MFJ1a4lTRR2OuAe8Hxa/3tgU5Taw=" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{ asset('css/style.css?v=2.0.7') }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js" integrity="sha256-uWtSXRErwH9kdJTIr1swfHFJn/d/WQ6s72gELOHXQGM=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/midnight.js/1.1.2/midnight.jquery.min.js" integrity="sha256-5ZPAqMrGzPozXZGh7GKgLZpbSBwAPQRAyYwcWNf8Q7c=" crossorigin="anonymous"></script>    
    <!-- Jarallax -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jarallax/1.9.2/jarallax.min.js"></script>
    <!-- Include it if you want to parallax any element -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jarallax/1.9.2/jarallax-element.min.js"></script>
    <script>var AESlogo = "{{ asset('img/Icons/YINGYANG.png') }}";</script>
    <script src="{{ asset('js/app.js?v=3.0.0') }}" type="application/javascript"></script>
    @yield('JSextra')    
    <a href="#" data-activates="slide-out" class="button-collapse menu-icon-open"><i class="material-icons">menu</i></a>
    <ul id="slide-out" class="side-nav">
        <li><a href="#!" class="close-side-nav">X</a></li>    
        <li><a id="logo-menu-aes" href="{{ url('/') }}"><img src="{{ asset('img/LogoAESSolucionesazul.png') }}" alt="AES" class="responsive-img"></a></li>    
        <li><a class="waves-effect" href="{{ url('/') }}">Inicio</a></li>
        <li><a class="waves-effect" href="{{ url('nuestra-empresa') }}">Nuestra Empresa</a></li>
        <li><a class="waves-effect" href="{{ url('solar') }}">Solar</a></li>
        <li><a class="waves-effect" href="{{ url('sustentable') }}">Sustentable</a></li>
        <li><a class="waves-effect" href="{{ url('servicios') }}">Servicios</a></li>
        <li><a class="waves-effect" href="{{ url('su-casa') }}">Su casa</a></li>
        <li><a class="waves-effect" href="{{ url('storage-aes') }}">Storage</a></li>
        <li><a class="waves-effect" href="{{ url('contactenos') }}">Contáctenos</a></li>
    </ul>
</body>
</html>
