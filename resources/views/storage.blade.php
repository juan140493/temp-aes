@extends('template.main')

@section('title', 'Storage')

@section('content')

<header class="storage-header bg-head-5">
  <div class="top">
    <a href="{{ url('/') }}">
      <figure>
        <img src="{{ asset('img/LogoAESSolucionesBN.png') }}" alt="AES Soluciones"> 
      </figure>
    </a>
  </div> 
  <h1>
    STORAGE
  </h1>   
</header>
<section id="main">
  
  <section class="row storage-section1">
    <div class="col s12 m10 offset-m1 l5 xl5 bg-white">
      <div class="cont-text-img">
        <img src="{{ asset('img/Icons/storage.png') }}" alt="AES">
        <h2>Servicio de<br>asistencia AES</h2>
        <p>       
          AES Soluciones ofrece a las empresas generadoras y a los operadores del sistema de transmisión, la instalación, operación y mantenimiento de Sistemas de Almacenamiento de Energía de respuesta rápida, o Energy Storage.
          <br><br>
          Entre los principales beneficios destacan:
          <br>
          - Reducción de deslastre de carga debido a fallas súbitas de generadores o inestabilidades de frecuencia.
          <br><br>
          - Disminución de la necesidad de operar costosas turbinas a gas, para responder a la salida temporal de líneas de transmisión y generadores.
          <br><br>
          - Reducción de pérdidas por transmisión
        </p>
      </div>
    </div>
    <div class="col s12 m12 l7 xl7 bg-blue">
      <canvas width="280" height="700" id="canvas-storage" class="hide-on-992"></canvas>
      <div class="cont-text-img">        
        <p>       
          Energy Storage, constribuye a mejorar la confiabilidad del sistema eléctrico, ya que permite suministrar energía a los usuarios ante fallas o eventualidades de la red.
          <br><br>
          Proporciona además considerables mejoras en la calidad de la energía, como la regulación de tensión y frecuencia, resolviendo así los problemas de fluctuaciones del sistema eléctrico.
          <br><br>
          Asi mismo, incrementa la disponibilidad del parque de generación y facilita la penetración de energías renovables.
          <br><br>
          Otra ventaja del servicio de almacenamiento de energía es que reduce los costos en los precios de la electricidad, al acumular la energía generada en los periodos de baja demanda cuyos precios son más bajos, con el objetivo de utilizarla en los periodos de máxima demanda donde los precios son mayores.
        </p>
      </div>      
    </div>
  </section>

</section>

@endsection
