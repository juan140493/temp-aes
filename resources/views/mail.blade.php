<h3>Nueva Información de contacto</h3> 
<br>
<table width="500">
	<tr>
		<td>Nombre:</td>
		<td>{{ $name }}</td>
	</tr>
	<tr>
		<td>Email:</td>
		<td>{{ $email }}</td>
	</tr>
	<tr>
		<td>Teléfono:</td>
		<td>{{ $phone ? $phone : '--' }}</td>
	</tr>
	<tr>
		<td colspan="2">Mensaje:</td>
	</tr>
	<tr>
		<td colspan="2">
			{{ $msg }}			
		</td>
	</tr>
</table>

