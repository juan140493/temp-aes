@extends('template.main')

@section('title', 'Nuestra Empresa')
@section('CSSextra')
  <style>
    .row.company-solutions-rows.bg-blue p{
      padding: 20px 0;
    }
    .row.company-solutions-rows.bg-blue .long-text{
      font-size: 1em;      
      width: 90%;
    }
  </style>
@endsection

@section('content')

<header class="company-header bg-head-2">
  <div class="top">
    <a href="{{ url('/') }}">
      <figure>
        <img src="{{ asset('img/LogoAESSolucionesBN.png') }}" alt="AES Soluciones"> 
      </figure>
    </a>
  </div> 
  <img src="{{ asset('img/composicion_nuestra_empresa.png') }}" alt="Te llevamos luz para que tu vida brille" class="img-title">
</header>
<section id="main">
  
  <section class="row company-solutions">
    <div class="col s12 m6 l7 xl7 right-align bg-ne2">
      <canvas height="620" width="350" id="canvas-soluciones"></canvas>
      <div class="show-on-700">AES Soluciones</div>
    </div>
    <div class="col s12 m6 l5 xl5 valign-wrapper bg-white">
      <span class="line-p"></span>
      <p>
        Somos una compañía que difunde impulsa tecnología a través de nuevas formas de uso energético solar, lumínica lumínico, de almacenamiento y de uso residencial. Tenemos soluciones de productos y servicios de infraestructura e instalaciones eléctricas que garantizan un mayor aprovechamiento de los recursos.
        <br><br>
        Impulsamos soluciones alternativas de energía, desarrollando proyectos de iluminación eficiente, generación de energía con fuentes renovables y almacenamiento de energía, que permite mejorar el funcionamiento operativo de la industria, comercio y servicios, generando una Huella Positiva sobre nuestro medio ambiente.
        <br><br>
        Además, ofrecemos productos y servicios de infraestructura e instalaciones eléctricas que garantizan un mayor aprovechamiento de los recursos, así como la eficiencia de los equipos en la industria.
        <br>
        Comuníquese con nosotros: <br>
        <a href="mailto:aes.soluciones@aes.com?subject=Contacto">aes.soluciones@aes.com</a>
      </p>
    </div>
  </section>
  <section class="row company-solutions-rows bg-blue">
    <div class="col s12 m4 l4 xl4">
      <p>
        Brindamos tecnología accesible y rentable para comercios e industrias con el objetivo de generar una mayor competitividad financiera y comercial a las empresas mientras contribuimos con las sostenibilidad medioambiental.
      </p>
    </div>
    <div class="col s12 m4 l4 xl4">
      <p class="long-text">
        También acompañamos a los gobiernos municipales en su búsqueda por convenir convertir sus municipios en ciudades sostenibles, a través de implementación de alumbrado público eficiente e inteligente inteligente, incrementando así la seguridad y el desarrollo económico y social del municipio. Nuestras soluciones ayudan a los municipios en la implementación de <i>Smart Cities</i> (ciudades inteligentes), gestionando eficiencia en muchas áreas de la ciudad, como el urbanismo, infraestructuras, transporte, servicios, educación, sanidad, seguridad pública y energía.
      </p>
    </div>
    <div class="col s12 m4 l4 xl4">
      <p>
        Atendemos el mercado residencial energético más importante del país. Servimos energía en 220 municipios(de un total de 262) abarcando todos los sectores económicos. Damos un servicio integral que incluye: <b>evaluación, asesoría, implementación y servicio post venta.</b>
      </p>
    </div>
  </section>
  <section class="container-services">
    <h2>Conozca más sobre nuestros servicios:</h2>
    <div class="row">
      <div class="col s12 m6 l4 xl4">
        <img src="{{ asset('img/Icons/creacion_diseno.png') }}" alt="Icon">
        <span>Solar</span>
        <p>
          Brindamos servicios de diseño, construcción y operación y mantenimiento (O&M) de plantas solares fotovoltaicas.
        </p>
      </div>
      <div class="col s12 m6 l4 xl4">
        <img src="{{ asset('img/Icons/linea_de_productos_.png') }}" alt="Icon">
        <span>Sustentable</span>
        <p>
          Contamos con una línea de productos y servicios de iluminación residencial, iluminación industrial e iluminación pública con altos estándares de calidad, que le permitirán lograr eficiencia energética a través de tecnologías de primer nivel.
        </p>
      </div>
      <div class="col s12 m6 l4 xl4">
        <img src="{{ asset('img/Icons/aire_acondicionado.png') }}" alt="Icon">
        <span>Aire acondicionado</span>
        <p>
          Además ofrecemos diferentes tipos de equipos de aires acondicionados de última generación, como mini splits, paquetes y centrales, para poder brindarle soluciones de ahorro de energía para su comercio o industria.
          <br>
          Ofrecemos soluciones que resuelvan las necesidades de control climático y ambiental.
        </p>
      </div>
      <div class="col s12 m6 l4 xl4">
        <img src="{{ asset('img/Icons/energy_storage.png') }}" alt="Icon">
        <span>Storage</span>
        <p>
          Con <i>Energy</i> Storage, nuestra plataforma de almacenamiento de energía, otorgamos respuesta de alta velocidad para entregar energía al sistema en cada instante requerido.
        </p>
      </div>
      <div class="col s12 m6 l4 xl4">
        <img src="{{ asset('img/Icons/ayuda_inmediata.png') }}" alt="Icon">        
        <span>Servicios</span>
        <p>
          Nuestros servicios técnicos le garantizan el buen estado funcional de todos los elementos que integran sus equipos en una subestación eléctrica, a través de mantenimientos preventivos, predictivos y correctivos.
        </p>
      </div>
      <div class="col s12 m6 l4 xl4">
        <img src="{{ asset('img/Icons/servicio_tecnico.png') }}" alt="Icon">
        <span>Su Casa</span>
        <p>
          Usted podrá recibir ayuda inmediata a través de personal calificado, que se encargará de asistirle en emergencias en su hogar, asistencia vial y legal, además podrá contar con atención médica programada a través de citas individuales.
        </p>
      </div>
    </div>
  </section>
  <section class="row us bg-gray">
    <div class="col s12 m6 l5 xl5">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header active"><span class="icon-us">+</span><span>Nuestra misión</span></div>
          <div class="collapsible-body">
            <span>
              Mejorar vidas a través de soluciones energéticas seguras, confiables y sostenibles en todos los mercados que servimos.
            </span>
          </div>
        </li>
        <li>
          <div class="collapsible-header"><span class="icon-us">+</span><span>Nuestra visión</span></div>
          <div class="collapsible-body">
            <span>
              Nuestra visión es ser la compañía de energía sostenible líder en el mundo que proporciona de manera segura energía confiable y asequible.
              <br> <a style="text-decoration: underline;" href="{{ url('/nosotros') }}" alt="Ver mas">Ver más</a>
            </span>
          </div>
        </li>
        <li>
          <div class="collapsible-header"><span class="icon-us">+</span><span>Nuestros valores</span></div>
          <div class="collapsible-body">
            <span>
              Seguridad, Integridad, Agilidad, Excelencia, Divertido <br>
              <a style="text-decoration: underline;" href="{{ url('/nosotros#Valores') }}" alt="Ver mas">Ver más</a>
            </span>
          </div>
        </li>
        <li>
          <div class="collapsible-header"><span class="icon-us">+</span><span>Nuestra cultura</span></div>
          <div class="collapsible-body">
            <span>
              La energía que nuestra gente aporta a lo que hacen es lo que impulsa a AES. En esencia, nuestra cultura es cómo canalizamos esa energía a través de las formas en que trabajamos juntos. 
              <br>
              <a style="text-decoration: underline;" href="{{ url('/nosotros#Cultura') }}" alt="Ver mas">Ver más</a>
            </span>
          </div>
        </li>
      </ul>
    </div>
    <div class="col s12 m6 l7 xl7 bg-ne3">
      <canvas width="400" height="650" id="canvas-us"></canvas>
    </div>
  </section>
  <section class="row company-solutions info">
    <div class="col s12 m6 l8 xl8 right-align bg-ne4">
      <canvas height="620" width="350" id="canvas-info"></canvas>
      <div class="show-on-700">AES El Salvador</div>
    </div>
    <div class="col s12 m6 l4 xl4 valign-wrapper bg-white">
      <span class="line-p"></span>
      <p>
        A través de nuestras empresas, respondemos a las necesidades de la población brindando soluciones energéticas para el comercio y la industria de nuestro país, cumpliendo con nuestra responsabilidad de generar y distribuir energía eléctrica confiable y limpia. 
        <br><br>
        Promoviendo además su uso eficiente y seguro, e impulsando proyectos urbanos y rurales que permiten el crecimiento del sector productivo y mejoran la calidad de vida de las comunidades a las que atendemos. 
      </p>
    </div>
  </section>
  <section class="row green-info bg-green">
    <div class="col s10 offset-s1 m6 l6 xl6">
      <h2>Distribución confiable y segura</h2>
    </div>
    <div class="col s10 offset-s1 m6 l6 xl6">
      <p>
        A través de nuestras empresas distribuidoras ofrecemos energía eléctrica a los hogares, las escuelas, los hospitales, la industria y el comercio de diversos municipios y departamentos de nuestro país, contribuyendo a mejorar la calidad de vida de los salvadoreños. <br>
        <b>• CAESS: </b>Zona norte de San Salvador, Chalatenango, Cuscatlán y Cabañas <br>
        <b>• CLESA: </b>Santa Ana, Sonsonate, Ahuachapán y parte del departamento de La Libertad <br>
        <b>• EEO: </b>San Miguel, Morazán, La Unión y parte de Usulután y San Vicente <br>
        <b>• DEUSEM: </b>Usulután <br>
        Servimos el 80% del territorio nacional, entregando energía a más de un 1.4 millones de clientes.
      </p>
    </div>
  </section>
  <section class="container-services">
    <h2>Generación de energía verde</h2>
    <div class="row">
      <div class="col s12 m6 l4 xl4">
        <p>
          El medio ambiente es una de las grandes preocupaciones a nivel mundial, y esto no excluye a la sociedad salvadoreña, por eso somos conscientes de nuestra responsabilidad y trabajamos con el compromiso de acompañar a nuestro país en su crecimiento económico y social, buscando soluciones de energía con una perspectiva ambientalmente responsable. 
          <br><br>
          Gracias a nuestro crecimiento proyectado en generación de energía limpia, en los próximos dos años alcanzamos a retirar de la atmósfera el equivalente a ~380 mil toneladas métricas de dióxido de carbono (CO2).
        </p>
      </div>
      <div class="col s12 m6 l4 xl4">
        <p>
          <b>AES Moncagua*:</b> Es una planta de generación fotovoltaica con una capacidad instalada de 2.6MWp, producidos a través de más de 8 mil módulos fotovoltaicos de tipo policristalino. Reduce anualmente 2.700 TM de emisiones de CO2
          <br><br>
          <b>AES Nejapa:</b> Genera 6 MW de energía eléctrica a partir de biogás que se obtiene de la descomposición de los desechos sólidos en relleno sanitario. Reduce anualmente 200.000 TM de emisiones de CO2
        </p>
      </div>
      <div class="col s12 m6 l4 xl4">
        <p>
          Bósforo* (en construcción): Proyecto de 100 MW de energía con fuente sola fotovoltaica. Al final de su construcción reducirá el equivalente a ~176.00 TM de emisiones de CO2. 
          <br><br>
          AES Moncagua y AES Nejapa son operadas por AES Soluciones. Bósforo también será operada por nuestra empre soluciones energéticas AES Soluciones.
          <br><br>
          Visite la página web 
          <a href="http://www.aes-elsalvador.com" target="_blank">www.aes-elsalvador.com</a> 
        </p>
      </div>
    </div>
  </section>
  <div class="row bg-emp">
    <div class="col s12 m10 offset-m2 l8 offset-l4 xl8 offset-xl4 bg-semi">
      <div>
        <h2>Corporación<span>AES</span></h2>
        <br><br>
        La Corporación  AES es una de las principales compañías de energía del mundo. Cada día, en 17 países, nuestra fuerza laboral compuesta por más de 19,000 personas, genera y entrega la electricidad que hace posible el desarrollo de los negocios, brinda energía a hospitales y escuelas y elevan la calidad de vida de millones de personas alrededor del mundo. <br><br>
        Nuestra amplia experiencia de más de 30 años, nos permite identificar las necesidades de los países en que operamos, y ofrecer las alternativas energéticas idóneas según las diversas realidades económicas y geográficas.
        <br><br>
        Visite la página web <a href="http://www.aes.com">www.aes.com</a>
      </div>
    </div>
  </div>
</section>

@endsection
