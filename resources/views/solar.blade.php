@extends('template.main')

@section('title', 'Solar')

@section('content')

<header class="solar-header bg-head-3">
  <div class="top">
    <a href="{{ url('/') }}">
      <figure>
        <img src="{{ asset('img/LogoAESSolucionesBN.png') }}" alt="AES Soluciones"> 
      </figure>
    </a>
  </div> 
  <h1>
    <p>
      SOLAR
      <span>Sistemas fotovoltaicos</span>
    </p>
  </h1>   
</header>
<section id="main">
  
  <section class="row solar-section1">    
    <div class="col s12 m6 l5 xl5 valign-wrapper bg-gray">
      <span class="line-p"></span>
      <p>
        Innovamos con servicios de energía solar fotovoltaica, una fuente de energía renovable que disminuye el consumo de los combustibles fósiles, aportando a la diversificación de la matriz energética de nuestro país y contribuyendo a la sostenibilidad ambiental.
        <br><br>
        Los servicios de energía solar fotovoltaica son diversos y con estándares diseñados a la medida de los clientes del sector comercial, industrial y residencial.
        <br><br>
        A través de los servicios de energía solar fotovoltaica, AES Soluciones ofrece a los clientes el suministro de energía con un menor costo que los precios del mercado.
      </p>
    </div>
    <div class="col s12 m6 l7 xl7 left-align bg-s2">
      <canvas height="620" width="350" id="canvas-solar1"></canvas>
      <div class="show-on-700">AES Soluciones</div>
    </div>
  </section>
  <section class="row black-info bg-white">
    <div class="col s12 m6 l6 xl6">
      <h2>Sistemas solares fotovoltaicos (SFVs)</h2>
    </div>
    <div class="col s12 m6 l6 xl6 right-align">
      <p>
        Estos SFVs permiten la reducción del dióxido  de carbono (CO2), lo que les convierte en una alternativa energética amigable con el medio ambiente.
      </p>
    </div>
  </section>
  <section class="row solar-section2">    
    <div class="col s12 m6 l8 xl8 right-align bg-s3 icon-canvas">
      <canvas height="650" width="380" id="canvas-solar2"></canvas>      
    </div>
    <div class="col s12 m6 l4 xl4 valign-wrapper bg-blue">
      <span class="line-p"></span>
      <p>
        <b>Sistemas Solares Fotovoltaicos sobre Techos/ Rooftop y Carpot</b>
        <br><br>
        Desarrollamos proyectos de construcción de sistemas solares fotovoltaicos (SFVs) sobre techos de edificios – roof top- y en parqueos – carport – para lograr un autoabastecimiento de energía de sus instalaciones y obtener ahorros en su consumo de energía. Esto SFVs también permiten la reducción de dióxido de carbono (CO2), lo que les convierte en una alternativa energética amigable con el medio ambiente. 
        <br><br>
        AES Soluciones ejecutó proyectos de construcción de SFVs sobre techos en las instalaciones de CAESS, CLESA y EEO. Estos sistemas generan más de 150 KW de energía, obteniendo un ahorro aproximado a los US$ 30,000 al año y a la reducción de 188 toneladas de CO2 al año. 
      </p>
    </div>    
  </section>
  <section class="row bg-blue" id="aux-canvas">
    <div class="col s4"></div>
    <div class="col s4 w"></div>
    <div class="col s4"></div>
  </section>
  <section class="row solar-section3">        
    <div class="col s12 m12 l4 xl4 valign-wrapper bg-blue">
      <p>
        <b>Sistema Solares Fotovoltaicos sobre Piso (Granja Solares)</b>
        <br><br>
        Ponemos a su servicio nuestra experiencia en el desarrollo y construcción de sistemas solares fotovoltaicos (<b>SFV</b>) sobre piso a gran escala, también conocidos como Granjas Solares, para inyección y venta de la energía producida a la red de distribución a través de un contrato de abastecimiento de energía (<b>PPA</b>). 
        <br><br>
        En El Salvador, <b>AES</b> ha desarrollado el proyecto de <b>AES</b> Moncagua, una Granja Solar con una capacidad de 2.6MWp ubicada en el municipio de Moncagua, departamento de San Miguel.
        Adicionalmente, junto con su socio CMI, <b>AES</b> está desarrollando a los proyectos Bósforo, que consisten en diez plantas de 14MWp de potencia cada una, las cuales serán construidas en las 3 regiones de El Salvador.

      </p>
    </div> 
    <div class="col s12 m12 l8 xl8 left-align bg-white icon-canvas">
      <img src="{{ asset('img/Flores/flor_azul.png') }}" alt="AES" class="bot">
      <img src="{{ asset('img/Flores/flor_blanca.png') }}" alt="AES" class="top">
      <canvas height="650" width="380" id="canvas-solar3"></canvas>
      <div class="video side-right valign-wrapper">
        <iframe width="545" height="300" src="https://www.youtube.com/embed/kiVo6TcrKPo?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
      </div>
    </div>   
  </section>

  <section class="row solar-section4">    
    <div class="show-on-992-col col s12 m12 l4 xl4 valign-wrapper bg-green">
      <span class="line-p"></span>
      <p>        
        <b>Energía rural solar</b>
        <br><br>
        Como parte de nuestra responsabilidad social empresarial desarrollamos un proyecto piloto de energía rural solar, que benefició 14 familias de San Francisco Menéndez, en Ahuachapán. Esta iniciática, que deseamos expandir hacia otras zonas remotas del país, permitiría llevar energía a comunidades de difícil acceso y ubicadas a grandes distancias de las líneas de distribución eléctrica, de una manera más eficiente.
      </p>
    </div>    
    <div class="col s12 m12 l8 xl8 right-align bg-white">
      <div class="video side-left valign-wrapper">
        <iframe width="545" height="300" src="https://www.youtube.com/embed/7nAUI5ERPls?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
      </div>
      <canvas height="650" width="380" id="canvas-solar4"></canvas>
    </div>
    <div class="col s12 m12 l4 xl4 valign-wrapper bg-green hide-on-992">
      <span class="line-p"></span>
      <p>        
        <b>Energía rural solar</b>
        <br><br>
        Como parte de nuestra responsabilidad social empresarial desarrollamos un proyecto piloto de energía rural solar, que benefició 14 familias de San Francisco Menéndez, en Ahuachapán. Esta iniciática, que deseamos expandir hacia otras zonas remotas del país, permitiría llevar energía a comunidades de difícil acceso y ubicadas a grandes distancias de las líneas de distribución eléctrica, de una manera más eficiente.
      </p>
    </div>    
  </section>
  <section class="row bg-s4 solar-section5">
    <div class="col s12 m11 offset-m1 l8 offset-l4 xl8 offset-xl4 bg-semi">
      <div>
        <b>Operación y Mantenimiento (O&M)</b> <br><br>
        A través de AES Soluciones ponemos a disposición de nuestro clientes a los servicios de Operación y Mantenimiento (O&M) de sistemas solares fotovoltaicos (SFV), tanto sobre techos (Roof-top y Carport) como sobre piso (Granjas Solares). 
      </div>
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header active"><span class="icon-us">+</span><span>Supervisión del SFV</span></div>
          <div class="collapsible-body">
            - Detección y diagnóstico de problemas del SFV <br>
            - Monitoreo del rendimiento del SFV en tiempo real <br>
            - Notificaciones inmediatas al propietario del SFV <br>
            - Monitoreo de la seguridad del SFV
          </div>
        </li>
        <li>
          <div class="collapsible-header active"><span class="icon-us">+</span><span>Operación del SFV</span></div>
          <div class="collapsible-body">
            - Control a distancia del SFV<br>
            - Pronósticos de la generación esperada del SFV <br>
            - Programación de mantenimientos preventivos
          </div>
        </li>
        <li>
          <div class="collapsible-header active"><span class="icon-us">+</span><span>Ingeniería del Rendimiento del SFV</span></div>
          <div class="collapsible-body">
            - Análisis de tendencias del SFV <br>
            - Detección temprana de problemas de rendimiento
          </div>
        </li>
        <li>
          <div class="collapsible-header active"><span class="icon-us">+</span><span>Mantenimiento del SFV</span></div>
          <div class="collapsible-body">
            - Mantenimiento preventivo de los equipos (inversores, módulos, etc.) <br>
            - Reparación y reemplazo de equipos
          </div>
        </li>
        <li>
          <div class="collapsible-header active"><span class="icon-us">+</span><span>Back-Office</span></div>
          <div class="collapsible-body">
            - Reportes de producción y rendimiento del SFV <br>
            - Reportes de actividades de mantenimiento
          </div>
        </li>
      </ul>
    </div>
  </section>
</section>

@endsection
