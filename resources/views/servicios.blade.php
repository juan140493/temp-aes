@extends('template.main')

@section('title', 'Servicios')

@section('content')

<header class="services-header bg-head-6">
  <div class="top">
    <a href="{{ url('/') }}">
      <figure>
        <img src="{{ asset('img/LogoAESSolucionesBN.png') }}" alt="AES Soluciones"> 
      </figure>
    </a>
  </div> 
  <h1>
    SERVICIOS
  </h1>   
</header>
<section id="main">
  
  <section class="row services-section1">
    <div class="col s12 m6 l7 xl7 right-align bg-ser2">
      <canvas height="620" class="hide-on-700" width="350" id="canvas-soluciones"></canvas>
      <div class="show-on-700">AES Soluciones</div>
    </div>
    <div class="col s12 m6 l5 xl5 valign-wrapper bg-gray">
      <span class="line-p"></span>
      <p>
        La energía eléctrica es un insumo imprescindible para toda actividad productiva.
        <br><br>
        Por esa razón se vuelve fundamental analizar, diseñar, construir y brindar un mantenimiento calificado y oportuno a la infraestructura e instalaciones eléctricas de su industria o comercio.
        <br><br>
        De esta manera se garantiza un mayor aprovechamiento de sus recursos y la eficiencia de sus equipos para evitar accidentes o fallas que puedan afectar sus procesos de producción.
      </p>
    </div>
  </section>
  <section class="row white-info bg-blue">
    <div class="col s12 m6 l6 xl6">
      <h2>A nuestros clientes</h2>
    </div>
    <div class="col s12 m6 l6 xl6 right-align">
      <p>
        Nuestros servicios técnicos le garantizan el buen estado funcional de todos los elementos que integran sus equipos en una subestación eléctrica, a través de acciones preventivas, pruebas mecánicas, eléctricas y dieléctricas.
      </p>
    </div>
  </section>  
  <section class="container-services services-grid">
    <div class="row">
      <div class="col s12 m4 l4 xl4">
        <img src="{{ asset('img/Icons/mantenimiento.png') }}" alt="Icon">
        <span>Mantenimiento preventivo</span>
        <p>
          Cambio o reemplazo de partes en función de un intervalo de tiempo.
        </p>
      </div>
      <div class="col s12 m4 l4 xl4">
        <img src="{{ asset('img/Icons/mantenimiento_proactivo.png') }}" alt="Icon">
        <span>Mantenimiento proactivo</span>
        <p>
          Dirigido fundamentalmente a la detección de las causas que generan el desgaste y que conducen a las fallas.
        </p>
      </div>
      <div class="col s12 m4 l4 xl4">
        <img src="{{ asset('img/Icons/mantenimiento_predictivo.png') }}" alt="Icon">
        <span>Mantenimiento predictivo</span>
        <p>
          Indica el momento en que las piezas o componentes están próximos a finalizar su vida útil.
        </p>
      </div>
    </div>
  </section>
  <section class="row services-section2">
    <div class="col s12 m6 l4 xl4 valign-wrapper bg-gray">
      <ul>
        <li>Mantenimiento a Subestaciones Eléctricas</li>
        <li>Auditoría de Energía Eléctrica</li>
        <li>Análisis de la Demanda y Medición de Parámetros Eléctricos</li>
        <li>Pruebas de Resistencia de Tierra de la Subestación</li>
        <li>Análisis de Balance de Cargas</li>
        <li>Análisis y Medición de Corrientes Armónicas</li>
        <li>Análisis Termonográfico</li>
      </ul>
      <span class="more-info">Para mayor información contactarse con aes.soluciones@aes.com</span>
    </div>    
    <div class="col s12 m6 l8 xl8 valign-wrapper bg-green">
      <canvas id="canvas-services" height="650" width="350" class="hide-on-992"></canvas>
      <ul>
        <li>Estudio de Factor de Potencia</li>
        <li>Análisis del Aceite de los Transformadores</li>
        <li>Diseño y Construcción de Subestaciones Eléctricas</li>
        <li>Diseño y Construcción de Líneas Primarias (Media Tensión)</li>
        <li>Análisis de Cortocircuito</li>
        <li>Estudio de Coordinación de Protecciones</li>
        <li>Estudio de Estabilidad de los Sistemas Eléctricos</li>
      </ul>
    </div>
  </section>
</section>

@endsection
